package io.reborn.loanmanager.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class BlackList {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "person_id", referencedColumnName = "id")
    private Person person;

    public BlackList() {}

    public BlackList(Person person) {
        this();
        this.person = person;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlackList blackList = (BlackList) o;
        return id == blackList.id &&
                Objects.equals(person, blackList.person);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, person);
    }
}
