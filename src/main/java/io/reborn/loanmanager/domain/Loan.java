package io.reborn.loanmanager.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Loan {

    @Id
    @GeneratedValue
    private int id;
    private String term;
    private double amount;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="country_id")
    private Country country;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="person_id")
    private Person person;

    public Loan() {}

    public Loan(String term, double amount, Country country, Person person) {
        this();
        this.term = term;
        this.amount = amount;
        this.country = country;
        this.person = person;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Country getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return id == loan.id &&
                Double.compare(loan.amount, amount) == 0 &&
                Objects.equals(term, loan.term) &&
                Objects.equals(country, loan.country) &&
                Objects.equals(person, loan.person);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, term, amount, country, person);
    }

    public void setCountry(Country country) {
        this.country = country;
    }

}
