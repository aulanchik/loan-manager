package io.reborn.loanmanager.repository;

import io.reborn.loanmanager.domain.BlackList;
import io.reborn.loanmanager.domain.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlackListRepository extends CrudRepository<BlackList, Integer> {
    BlackList findByPerson(Person person);
}
