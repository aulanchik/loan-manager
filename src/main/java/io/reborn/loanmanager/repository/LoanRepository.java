package io.reborn.loanmanager.repository;

import io.reborn.loanmanager.domain.Loan;
import io.reborn.loanmanager.domain.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanRepository extends CrudRepository<Loan, Integer> {
    List<Loan> findByPerson(Person person);
}
