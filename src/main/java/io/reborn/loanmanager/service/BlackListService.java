package io.reborn.loanmanager.service;

public interface BlackListService {
    boolean isBlackListPerson(int personId);
}
