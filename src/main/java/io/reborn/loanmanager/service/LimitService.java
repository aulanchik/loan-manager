package io.reborn.loanmanager.service;

public interface LimitService {
    boolean isLimit(String locale);
}
