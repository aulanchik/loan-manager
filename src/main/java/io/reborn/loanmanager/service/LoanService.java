package io.reborn.loanmanager.service;

import io.reborn.loanmanager.domain.Loan;

import java.util.List;

public interface LoanService {

    Loan apply(Loan loan);

    List<Loan> getAll();

    List<Loan> getByPerson(int personId);
}
