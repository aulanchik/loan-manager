package io.reborn.loanmanager.service.impl;

import io.reborn.loanmanager.domain.Person;
import io.reborn.loanmanager.repository.BlackListRepository;
import io.reborn.loanmanager.service.BlackListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlackListServiceImpl implements BlackListService {

    private BlackListRepository blackListRepository;

    @Autowired
    public BlackListServiceImpl(BlackListRepository blackListRepository) {
        this.blackListRepository = blackListRepository;
    }

    @Override
    public boolean isBlackListPerson(int personId) {
        return blackListRepository.findByPerson(new Person(personId)) != null;
    }
}
