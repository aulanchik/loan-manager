package io.reborn.loanmanager.service.impl;

import io.reborn.loanmanager.domain.Loan;
import io.reborn.loanmanager.domain.Person;
import io.reborn.loanmanager.repository.LoanRepository;
import io.reborn.loanmanager.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoanServiceImpl implements LoanService {

    private final LoanRepository loanRepository;

    @Autowired
    public LoanServiceImpl(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Override
    public Loan apply(Loan loan) {
        return loanRepository.save(loan);
    }

    @Override
    public List<Loan> getAll() {
        return (List<Loan>) loanRepository.findAll();
    }

    @Override
    public List<Loan> getByPerson(int personId) {
        return loanRepository.findByPerson(new Person(personId));
    }
}
