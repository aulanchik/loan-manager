package io.reborn.loanmanager.web;

import io.reborn.loanmanager.domain.Loan;
import io.reborn.loanmanager.service.BlackListService;
import io.reborn.loanmanager.service.LoanService;
import io.reborn.loanmanager.web.messages.Error;
import io.reborn.loanmanager.web.messages.Result;
import io.reborn.loanmanager.web.messages.Success;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LoanController {

    private LoanService loanService;
    private BlackListService blackListService;

    @Autowired
    public LoanController(LoanService loanService, BlackListService blackListService) {
        this.loanService = loanService;
        this.blackListService = blackListService;
    }

    @PostMapping
    public Result apply(@RequestBody Loan loan) {
        final Result result;
        int personId = loan.getPerson().getId();
        if(!blackListService.isBlackListPerson(personId)) {
            result = new Success<>(loanService.apply(loan));
        } else {
            result = new Error<>(String.format("User %s in blacklist", personId));
        }
        return result;
    }

    @GetMapping("/")
    public List<Loan> getLoans() {
        return loanService.getAll();
    }

    @GetMapping("/{personId}")
    public List<Loan> getLoanHistory(@PathVariable("personId") int personId) {
        return loanService.getByPerson(personId);
    }
}
