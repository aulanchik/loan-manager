package io.reborn.loanmanager.web.messages;

public class Error<T> extends Result {

    private final T value;

    public Error(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
