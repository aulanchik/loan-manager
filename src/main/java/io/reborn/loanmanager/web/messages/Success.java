package io.reborn.loanmanager.web.messages;

public class Success<T> extends Result {

    private final T value;

    public Success(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
