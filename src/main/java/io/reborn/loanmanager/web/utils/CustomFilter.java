package io.reborn.loanmanager.web.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.reborn.loanmanager.service.LimitService;
import io.reborn.loanmanager.web.messages.Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.Locale;

@Component
@ConfigurationProperties(prefix = "country")
public class CustomFilter implements Filter {

    private final LimitService service;
    private final ObjectMapper mapper = new ObjectMapper();

    @Value("{default}") private String defaultCountry;

    @Autowired
    public CustomFilter(final LimitService service) {
        this.service = service;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        Locale locale = req.getLocale();
        if (!this.service.isLimit(locale != null ? locale.getCountry() : defaultCountry)) {
            chain.doFilter(req, res);
        } else {
            res.getOutputStream().write(
                    mapper.writeValueAsBytes(new Error<>("Exceed execute from this country"))
            );
        }
    }

    @Override
    public void destroy() { }
}
