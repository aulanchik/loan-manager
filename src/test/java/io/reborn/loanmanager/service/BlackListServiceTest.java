package io.reborn.loanmanager.service;

import io.reborn.loanmanager.domain.BlackList;
import io.reborn.loanmanager.domain.Person;
import io.reborn.loanmanager.repository.BlackListRepository;
import io.reborn.loanmanager.repository.PersonRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BlackListServiceTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private BlackListRepository blackListRepository;

    @Autowired
    private BlackListService blackListService;

    @Test
    public void whenPersonInBlackListThenReturnTrue() {
        Person person = new Person("Arthur", "Copperfield");
        personRepository.save(person);
        blackListRepository.save(new BlackList(person));
        boolean result = blackListService.isBlackListPerson(person.getId());
        assertTrue(result);
    }

    // hibernate Transient Object exception
    @Test
    @Ignore
    public void whenBlackListEmptyThenAnyPersonNotIn() {
        Person person1 = new Person("Arthur", "Copperfield1");
        boolean result = blackListService.isBlackListPerson(person1.getId());
        assertTrue(result);
    }
}
