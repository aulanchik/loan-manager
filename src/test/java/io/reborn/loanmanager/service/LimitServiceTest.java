package io.reborn.loanmanager.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LimitServiceTest {

    @Autowired
    private LimitService limitService;

    @Test
    public void whenLimitNotExceedThenFalse() {
        boolean result = limitService.isLimit("lv");
        assertFalse(result);
    }

    @Test
    public void whenLimitExceedThenFalse() {
        boolean result = limitService.isLimit("lv");
        assertTrue(result);
    }
}
