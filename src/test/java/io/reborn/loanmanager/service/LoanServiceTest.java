package io.reborn.loanmanager.service;

import io.reborn.loanmanager.domain.Country;
import io.reborn.loanmanager.domain.Loan;
import io.reborn.loanmanager.domain.Person;
import io.reborn.loanmanager.utils.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LoanServiceTest {

    @Autowired
    private LoanService loanService;

    @Test
    public void whenApplyLoadThenSaveInDb() {
        Person person = TestUtils.personsList().get(0);
        Country country = TestUtils.countryList().get(0);
        System.out.println(person.toString());
        Loan loan = loanService.apply(new Loan("test loan", 0D, country, person));
        List<Loan> result = loanService.getAll();
        assertTrue(result.contains(loan));
    }

    @Test
    public void whenFindByPersonThenReturnListOnlyForRerson() {
        Person person = TestUtils.personsList().get(0);
        Country country = TestUtils.countryList().get(0);
        Loan loan = this.loanService.apply(new Loan("test loan", 0D, country, person));
        List<Loan> result = this.loanService.getByPerson(person.getId());
        assertThat(result.iterator().next(), is(loan));
    }

}
