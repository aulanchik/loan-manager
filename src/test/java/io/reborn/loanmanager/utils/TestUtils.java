package io.reborn.loanmanager.utils;

import io.reborn.loanmanager.domain.Country;
import io.reborn.loanmanager.domain.Person;

import java.util.Arrays;
import java.util.List;

public final class TestUtils {

    public static List<Person> personsList() {
        return Arrays.asList(
                new Person("John", "Smith"),
                new Person("Arthur", "Copperfield")
        );
    }

    public static List<Country> countryList() {
        return Arrays.asList(
                new Country("Bulgaria"),
                new Country("Lithuania"),
                new Country("Estonia"),
                new Country("Russia"),
                new Country("United Kingdom"),
                new Country("Ukraine")
        );
    }
}
